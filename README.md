LibertyCode_Event
=================



Description
-----------

Library contains event components, 
to use event system to run executable, 
from specified event key or event name.

---



Requirement
-----------

- Script language: PHP: version 7 || 8

---



Installation
------------

Several ways are possible:

#### Composer

1. Requirement
    
    It requires composer installation.
    For more information: https://getcomposer.org
    
2. Command: Move in project root path
    
    ```sh
    cd "<project_root_path>"
    ```

3. Command: Installation
    
    ```sh
    php composer.phar require liberty_code/event ["<version>"]
    ```
    
4. Note

    - Include vendor
        
        If project uses composer, 
        vendor must be included:
        
        ```php
        require_once('<project_root_path>/vendor/autoload.php');
        ```
    
    - Configuration
    
        Installation command allows to add, 
        on composer file "<project_root_path>/composer.json",
        following configuration:
        
        ```json
        {
            "require": {
                "liberty_code/event": "<version>"
            }
        }
        ```

#### Include

1. Download
    
    - Download following repository.
    - Put it on repository root path.
    
2. Include source
    
    ```php
    require_once('<repository_root_path>/include/Include.php');
    ```

---



Usage
-----

#### Event

Events system allows to get executable, 
from specific configuration, 
and specified event key or event name.

_Elements_

- Event

    Allows to design an event, 
    who is an item containing unique key and configuration array, 
    to get executable (callback function) 
    from specified string key or string name.

- EventCollection

    Allows to design collection of events.
    Uses list of events, 
    to retrieve executable, 
    from specified event or string event name.

- EventFactory

    Allows to design an event factory, 
    to provide new or specified event instance,
    from specified configuration.
    
- DiEventFactory

    Extends event factory features. 
    Provides new event instance, 
    from specified DI provider.
    
- StandardEventFactory

    Extends DI event factory features. 
    Provides event instance.
    
_Example_

```php
// Get event factory
use liberty_code\event\event\factory\standard\model\StandardEventFactory;
$eventFactory = new StandardEventFactory($provider);
...
// Get new event from configuration
$event = $eventFactory->getObjEvent(array(...));
...
```

#### Builder

Builder allows to hydrate event collection, with events.

_Elements_

- DefaultBuilder
    
    Uses array of source data to hydrate event collection.

_Example_

```php
// Get event collection
use liberty_code\event\event\model\DefaultEventCollection;
$eventCollection = new DefaultEventCollection();
...
// Get event builder
use liberty_code\event\build\model\DefaultBuilder;
$eventBuilder = new DefaultBuilder($eventFactory);
...
// Hydrate event collection
$eventBuilder->setTabDataSrc(array(...));
$eventBuilder->hydrateEventCollection($eventCollection);
...
foreach($eventCollection->getTabKey() as $key) {
    echo($eventCollection->getObjEvent($key)->getStrKey() .'<br />');
}
/**
 * Show: 
 * Event key 1
 * ...
 * Event key N
 */
...
```

#### Observer

Observer allows to run executable (callback function), 
using event collection.

_Example_

```php
use liberty_code\event\observer\model\DefaultObserver;
$observer = new DefaultObserver();
$observer->setEventCollection(...);
...
// Run executable, from specified string event name, if exists
var_dump($observer->dispatch(...));
...
```

#### Call

Calls using event system,
to get callback function, 
to execute specific event destination.

_Elements_

- DefaultCall

    Extends call features. 
    Uses event observer, 
    to get executable,
    to execute specific destination.
    
- EventCall

    Extends default event call features. 
    Allows to configure event destination.
    
- EventCallFactory

    Extends call factory features. 
    Provides event call instance.
    
---


