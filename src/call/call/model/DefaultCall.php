<?php
/**
 * Description :
 * This class allows to define default event call class.
 * Default event call is a call, allows to execute specific configured destination,
 * from specified event observer.
 *
 * Default event call uses the following specified configuration:
 * [
 *     Default call configuration
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\event\call\call\model;

use liberty_code\call\call\model\DefaultCall as BaseDefaultCall;

use liberty_code\event\observer\api\ObserverInterface;
use liberty_code\event\call\call\library\ConstCall;
use liberty_code\event\call\call\exception\ObserverInvalidFormatException;



/**
 * @method ObserverInterface getObjObserver() Get event observer object.
 * @method void setObjObserver(ObserverInterface $objObserver) Set event observer object.
 */
abstract class DefaultCall extends BaseDefaultCall
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param ObserverInterface $objObserver
     */
    public function __construct(
        ObserverInterface $objObserver,
        array $tabConfig = null,
        array $tabCallConfig = null,
        callable $callableBefore = null,
        callable $callableAfter = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $tabConfig,
            $tabCallConfig,
            $callableBefore,
            $callableAfter
        );

        // Init router
        $this->setObjObserver($objObserver);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstCall::DATA_KEY_DEFAULT_OBSERVER))
        {
            $this->__beanTabData[ConstCall::DATA_KEY_DEFAULT_OBSERVER] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstCall::DATA_KEY_DEFAULT_OBSERVER
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstCall::DATA_KEY_DEFAULT_OBSERVER:
                    ObserverInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



}