<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\event\call\call\factory\library;



class ConstEventCallFactory
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_OBSERVER = 'objObserver';



    // Type configuration
    const CALL_CONFIG_TYPE_EVENT = 'event';
}