<?php
/**
 * Description :
 * This class allows to define standard event factory class.
 * Standard event factory allows to provide and hydrate event instance.
 *
 * Standard event factory uses the following specified configuration, to get and hydrate event:
 * [
 *     -> Configuration key(optional): "event key"
 *     type(required): "default",
 *     Default event configuration (@see DefaultEvent )
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\event\event\factory\standard\model;

use liberty_code\event\event\factory\model\DefaultEventFactory;

use liberty_code\event\event\library\ConstEvent;
use liberty_code\event\event\model\DefaultEvent;
use liberty_code\event\event\factory\standard\library\ConstStandardEventFactory;



class StandardEventFactory extends DefaultEventFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods getters
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabConfigFormat(array $tabConfig, $strConfigKey = null)
    {
        // Init var
        $result = parent::getTabConfigFormat($tabConfig, $strConfigKey);

        // Format configuration, if required
        if(!is_null($strConfigKey))
        {
            // Add configured key as event key, if required
            if(!array_key_exists(ConstEvent::TAB_CONFIG_KEY_KEY, $result))
            {
                $result[ConstEvent::TAB_CONFIG_KEY_KEY] = $strConfigKey;
            }
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getStrEventClassPathFromType($strConfigType)
    {
        // Init var
        $result = null;

        // Get class path of route, from type
        switch($strConfigType)
        {
            case null:
            case ConstStandardEventFactory::CONFIG_TYPE_DEFAULT:
                $result = DefaultEvent::class;
                break;
        }

        // Return result
        return $result;
    }

	
	
}