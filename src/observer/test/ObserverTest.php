<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load test
require_once($strRootAppPath . '/src/build/test/EventBuilderTest.php');

// Use
use liberty_code\event\event\model\DefaultEventCollection;
use liberty_code\event\observer\model\DefaultObserver;



// Init var
$objEventCollection = new DefaultEventCollection($objCallFactory);
$objObserver = new DefaultObserver($objEventCollection);


