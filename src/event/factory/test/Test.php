<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load test
require_once($strRootAppPath . '/src/event/factory/test/EventFactoryTest.php');

// Use
use liberty_code\event\event\model\DefaultEventCollection;
use liberty_code\event\event\model\DefaultEvent;



// Init var
$objEventCollection = new DefaultEventCollection($objCallFactory);
$objEventFactory->setObjEventCollection($objEventCollection);



// Test new event
$tabEventData = array(
    [
        [
            'key' => 'event_1',
            'call' => [
                'class_path_pattern' => 'liberty_code\\event\\event\\test\\ControllerTest1:action',
                //'class_path_format_require' => true
            ]
        ],
        'event_1_not_care'
    ], // Ok

    [
        [
            'type' => 'default',
            'name' => [],
            'call' => [
                'type' => 'dependency',
                'dependency_key_pattern' => 'svc_1:action'
            ]
        ],
        'event_2'
    ], // Ok

    [
        [
            'name' => ['event-a', 'event-b'],
            'call' => [
                'type' => 'file',
                'file_path_pattern' => 'src/event/test/FileControllerTest1.php',
                'file_path_format_require' => 7
            ]
        ],
        'event_3'
    ], // Ok

    [
        [
            'type' => 'test',
            'key' => 'event_4',
            'name' => ['event-a', 'event-c'],
            'call' => [
                'type' => 'function',
                'file_path_pattern' => '/src/event/test/FileControllerTest2.php:runAction',
                'file_path_format_require' => true
            ]
        ],
        'event_4_not_care'
    ], // Ko: default event config provide for unknown event

    [
        [
            'type' => 'default',
            'key' => 'event_4',
            'name' => ['event-a', 'event-c'],
            'call' => [
                'type' => 'function',
                'file_path_pattern' => '/src/event/test/FileControllerTest2.php:runAction',
                'file_path_format_require' => true
            ]
        ],
        'event_4_not_care'
    ], // Ok

    [
        [
            'call' => [
                'class_path_pattern' => 'liberty_code\\event\\event\\test\\ControllerTest1:action'
            ]
        ],
        'event_1',
        new DefaultEvent()
    ] // Ok
);

foreach($tabEventData as $eventData)
{
    echo('Test new event: <br />');
    echo('<pre>');var_dump($eventData);echo('</pre>');

    try{
        $tabConfig = $eventData[0];
        $strConfigKey = (isset($eventData[1]) ? $eventData[1] : null);
        $objEvent = (isset($eventData[2]) ? $eventData[2] : null);
        $objEvent = $objEventFactory->getObjEvent($tabConfig, $strConfigKey, $objEvent);

        echo('Class path: <pre>');var_dump($objEventFactory->getStrEventClassPath($tabConfig, $strConfigKey));echo('</pre>');

        if(!is_null($objEvent))
        {
            echo('Event class path: <pre>');var_dump(get_class($objEvent));echo('</pre>');
            echo('Event key: <pre>');var_dump($objEvent->getStrKey());echo('</pre>');
            echo('Event name: <pre>');var_dump($objEvent->getTabName());echo('</pre>');
            echo('Event config: <pre>');var_dump($objEvent->getTabConfig());echo('</pre>');
            echo('Event call: <pre>');var_dump($objEvent->getObjCall());echo('</pre>');
        }
        else
        {
            echo('Event not found<br />');
        }

    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . ':' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');


