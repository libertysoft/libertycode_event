<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\event\event\exception;

use liberty_code\event\event\library\ConstEvent;



class ConfigInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct($config);
		
		// Init var
		$this->message = sprintf
        (
            ConstEvent::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 10, "...")
        );
	}





	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init string index array check function
        $checkTabStrIsValid = function(array $tabStr)
        {
            $result = true;
            $tabStr = array_values($tabStr);

            // Check each value is valid
            for($intCpt = 0; ($intCpt < count($tabStr)) && $result; $intCpt++)
            {
                $strValue = $tabStr[$intCpt];
                $result = (
                    // Check valid string
                    is_string($strValue) &&
                    (trim($strValue) != '')
                );
            }

            return $result;
        };

        // Init var
        $result =
            // Check valid key
            (
                (!isset($config[ConstEvent::TAB_CONFIG_KEY_KEY])) ||
                (
                    is_string($config[ConstEvent::TAB_CONFIG_KEY_KEY]) &&
                    (trim($config[ConstEvent::TAB_CONFIG_KEY_KEY]) != '')
                )
            ) &&

            // Check valid name
            (
                (!isset($config[ConstEvent::TAB_CONFIG_KEY_NAME])) ||
                (
                    is_array($config[ConstEvent::TAB_CONFIG_KEY_NAME]) &&
                    $checkTabStrIsValid($config[ConstEvent::TAB_CONFIG_KEY_NAME])
                )
            ) &&

            // Check valid destination configuration
            isset($config[ConstEvent::TAB_CONFIG_KEY_CALL]) &&
            is_array($config[ConstEvent::TAB_CONFIG_KEY_CALL]) &&
            (count($config[ConstEvent::TAB_CONFIG_KEY_CALL]) > 0) &&

            // Check valid order
            (
                (!isset($config[ConstEvent::TAB_CONFIG_KEY_ORDER])) ||
                is_int($config[ConstEvent::TAB_CONFIG_KEY_ORDER])
            ) &&

            // Check valid default sort comparison analysis
            (
                (!isset($config[ConstEvent::TAB_CONFIG_KEY_SORT_COMPARE_DEFAULT])) ||
                (
                    is_int($config[ConstEvent::TAB_CONFIG_KEY_SORT_COMPARE_DEFAULT]) &&
                    in_array(
                        $config[ConstEvent::TAB_CONFIG_KEY_SORT_COMPARE_DEFAULT],
                        ConstEvent::getTabSortCompare()
                    )
                )
            ) &&

            // Check valid sort comparison by key option
            (
                (!isset($config[ConstEvent::TAB_CONFIG_KEY_SORT_COMPARE_USE_KEY])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstEvent::TAB_CONFIG_KEY_SORT_COMPARE_USE_KEY]) ||
                    is_int($config[ConstEvent::TAB_CONFIG_KEY_SORT_COMPARE_USE_KEY]) ||
                    (
                        is_string($config[ConstEvent::TAB_CONFIG_KEY_SORT_COMPARE_USE_KEY]) &&
                        ctype_digit($config[ConstEvent::TAB_CONFIG_KEY_SORT_COMPARE_USE_KEY])
                    )
                )
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($config)
    {
        // Init var
        $result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

        // Throw exception if check not pass
        if(!$result)
        {
            throw new static((is_array($config) ? serialize($config) : $config));
        }

        // Return result
        return $result;
    }
	
	
	
}