<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load test
require_once($strRootAppPath . '/observer/test/ObserverTest.php');

// Use
use liberty_code\call\call\library\ConstCall;
use liberty_code\event\call\call\event\library\ConstEventCall;
use liberty_code\event\call\call\event\model\EventCall;



// Init var
$tabDataSrc = array(
    'event_1_not_care' => [
        'key' => 'event_1',
        'call' => [
            'class_path_pattern' => 'liberty_code\\event\\event\\test\\ControllerTest1:action',
            //'class_path_format_require' => true
        ]
    ],
    'event_2' => [
        'type' => 'default',
        'name' => [],
        'call' => [
            'type' => 'dependency',
            'dependency_key_pattern' => 'svc_1:%1$s'
        ]
    ],
    'event_3' => [
        'name' => ['event-a', 'event-b'],
        'call' => [
            'type' => 'file',
            'file_path_pattern' => 'event/test/FileControllerTest1.php',
            'file_path_format_require' => 7
        ]
    ],
    [
        'type' => 'default',
        'name' => ['event-a', 'event-c'],
        'call' => [
            'type' => 'function',
            'file_path_pattern' => '/event/test/FileControllerTest2.php:runAction',
            'file_path_format_require' => true
        ]
    ],
    'event_5_not_care' => [
        'key' => 'event_5',
        'name' => ['event-a', 'event-b'],
        'call' => [
            'type' => 'class',
            'class_path_pattern' => 'liberty_code\\event\\event\\test\\Controller%1$s',
            'method_name_pattern' => '%2$s'
        ]
    ],
    'event_6' => [
        'type' => 'default',
        'name' => ['event-a', 'event-c'],
        'call' => [
            'type' => 'class',
            'class_path_pattern' => 'liberty_code\\event\\event\\test\\Controller%1$s',
            'method_name_pattern' => '%2$s'
        ]
    ]
);

$objEventBuilder->setTabDataSrc($tabDataSrc);
$objEventBuilder->hydrateEventCollection($objEventCollection);

$tabConfig = array(
    ConstCall::TAB_CONFIG_KEY_ELM_REPLACE_TYPE => ConstCall::ELM_REPLACE_TYPE_CONFIG_KEY_VALUE,
    ConstCall::TAB_CONFIG_KEY_ELM_FORMAT_KEY_PATTERN => '<%1$s>'
);

$callableBefore = function (array $tabParam) {
    echo('Test callable before:<pre>');var_dump($tabParam);echo('</pre>');
};

$callableAfter = function (array $tabParam) {
    echo('Test callable after: <pre>');var_dump($tabParam);echo('</pre>');
};

$objCall = new EventCall(
    $objObserver,
    $tabConfig,
    null,
    $callableBefore,
    $callableAfter
);



// Test call
$tabCallData = array(
    [
        [
            ConstEventCall::TAB_CALL_CONFIG_KEY_EVENT_KEY_PATTERN => 'event_2'
        ],
        [
            'strAdd2' => 'Value 1',
            'strAdd' => 'Value 2'
        ],
        [
            'action'
        ]
    ], // Ok

    [
        [
            ConstEventCall::TAB_CALL_CONFIG_KEY_EVENT_KEY_PATTERN => 'event_test'
        ],
        [
            'strAdd2' => 'Value 1',
            'strAdd' => 'Value 2'
        ],
        [
            'action'
        ]
    ], // Ko: event not found

    [
        [
            ConstEventCall::TAB_CALL_CONFIG_KEY_EVENT_KEY_PATTERN => 'event_<elm_key_1>'
        ],
        [
            'Value 1',
            'Value 2'
        ],
        [
            'action',
            'elm_key_1' => '2'
        ]
    ], // Ok

    [
        [
            ConstEventCall::TAB_CALL_CONFIG_KEY_EVENT_KEY_PATTERN => 'event_%1$s'
        ],
        [
            'Value 1',
            'Value 2'
        ],
        [
            'elm_key_1' => '1'
        ],
        [
            ConstCall::TAB_CONFIG_KEY_ELM_REPLACE_TYPE => ConstCall::ELM_REPLACE_TYPE_CONFIG_INDEX,
            ConstCall::TAB_CONFIG_KEY_ELM_FORMAT_KEY_PATTERN => '<%1$s>'
        ]
    ], // Ko: Bad configuration format

    [
        [
            ConstEventCall::TAB_CALL_CONFIG_KEY_EVENT_KEY_PATTERN => 'event_%1$s'
        ],
        [
            'Value 1',
            'Value 2'
        ],
        [
            'elm_key_1' => '1'
        ],
        [
            ConstCall::TAB_CONFIG_KEY_ELM_REPLACE_TYPE => ConstCall::ELM_REPLACE_TYPE_CONFIG_INDEX
        ]
    ] // Ok
);

foreach($tabCallData as $callData)
{
    echo('Test call: <br />');
    echo('<pre>');var_dump($callData);echo('</pre>');

    try{
        $tabCallConfig = $callData[0];
        $tabArg = $callData[1];
        $tabStrElm = $callData[2];

        $tabConfigPrevious = null;
        if(isset($callData[3]))
        {
            $tabConfigPrevious = $objCall->getTabConfig();
            $tabConfig = $callData[3];

            $objCall->setConfig($tabConfig);
            echo('Config: <pre>');var_dump($objCall->getTabConfig());echo('</pre>');
        }

        $objCall->setCallConfig($tabCallConfig);
        echo('Call config: <pre>');var_dump($objCall->getTabCallConfig());echo('</pre>');

        $callable = $objCall->getCallable($tabArg, $tabStrElm);
        echo('Call:<br />');print_r($callable());
        echo('<br /><br />');

        $callable = $objCall->getCallable($tabArg, $tabStrElm, false, false);
        echo('Call (without before, after):<br />');print_r($callable());

        if(!is_null($tabConfigPrevious))
        {
            $objCall->setConfig($tabConfigPrevious);
        }

    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . ':' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');


