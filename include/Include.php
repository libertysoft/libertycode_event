<?php

// Init var
$strRootPath = dirname(__FILE__) . '/..';

// Include class
include($strRootPath . '/src/event/library/ConstEvent.php');
include($strRootPath . '/src/event/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/event/exception/EventCollectionInvalidFormatException.php');
include($strRootPath . '/src/event/exception/CollectionConfigInvalidFormatException.php');
include($strRootPath . '/src/event/exception/CollectionKeyInvalidFormatException.php');
include($strRootPath . '/src/event/exception/CollectionValueInvalidFormatException.php');
include($strRootPath . '/src/event/api/EventInterface.php');
include($strRootPath . '/src/event/api/EventCollectionInterface.php');
include($strRootPath . '/src/event/model/DefaultEvent.php');
include($strRootPath . '/src/event/model/DefaultEventCollection.php');

include($strRootPath . '/src/event/factory/library/ConstEventFactory.php');
include($strRootPath . '/src/event/factory/exception/FactoryInvalidFormatException.php');
include($strRootPath . '/src/event/factory/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/event/factory/api/EventFactoryInterface.php');
include($strRootPath . '/src/event/factory/model/DefaultEventFactory.php');

include($strRootPath . '/src/event/factory/standard/library/ConstStandardEventFactory.php');
include($strRootPath . '/src/event/factory/standard/model/StandardEventFactory.php');

include($strRootPath . '/src/build/library/ConstBuilder.php');
include($strRootPath . '/src/build/exception/FactoryInvalidFormatException.php');
include($strRootPath . '/src/build/exception/DataSrcInvalidFormatException.php');
include($strRootPath . '/src/build/api/BuilderInterface.php');
include($strRootPath . '/src/build/model/DefaultBuilder.php');

include($strRootPath . '/src/observer/library/ConstObserver.php');
include($strRootPath . '/src/observer/exception/EventCollectionInvalidFormatException.php');
include($strRootPath . '/src/observer/exception/EventNotFoundException.php');
include($strRootPath . '/src/observer/exception/CallableUnableGetException.php');
include($strRootPath . '/src/observer/api/ObserverInterface.php');
include($strRootPath . '/src/observer/model/DefaultObserver.php');

include($strRootPath . '/src/call/call/library/ConstCall.php');
include($strRootPath . '/src/call/call/exception/ObserverInvalidFormatException.php');
include($strRootPath . '/src/call/call/model/DefaultCall.php');

include($strRootPath . '/src/call/call/event/library/ConstEventCall.php');
include($strRootPath . '/src/call/call/event/exception/CallConfigInvalidFormatException.php');
include($strRootPath . '/src/call/call/event/model/EventCall.php');

include($strRootPath . '/src/call/call/factory/library/ConstEventCallFactory.php');
include($strRootPath . '/src/call/call/factory/model/EventCallFactory.php');