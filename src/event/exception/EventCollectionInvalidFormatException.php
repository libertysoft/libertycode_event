<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\event\event\exception;

use liberty_code\event\event\library\ConstEvent;
use liberty_code\event\event\api\EventCollectionInterface;



class EventCollectionInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $eventCollection
     */
	public function __construct($eventCollection)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstEvent::EXCEPT_MSG_EVENT_COLLECTION_INVALID_FORMAT,
            mb_strimwidth(strval($eventCollection), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified event collection has valid format
	 * 
     * @param mixed $eventCollection
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($eventCollection)
    {
		// Init var
		$result = (
			(is_null($eventCollection)) ||
			($eventCollection instanceof EventCollectionInterface)
		);
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($eventCollection);
		}
		
		// Return result
		return $result;
    }
	
	
	
}