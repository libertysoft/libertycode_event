<?php
/**
 * Description :
 * This class allows to describe behavior of event factory class.
 * Event factory allows to provide new or specified event instance,
 * hydrated with a specified configuration,
 * from a set of potential predefined event types.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\event\event\factory\api;

use liberty_code\event\event\api\EventInterface;



interface EventFactoryInterface
{
    // ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get string class path of event,
     * from specified configuration.
     *
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @return null|string
     */
    public function getStrEventClassPath(array $tabConfig, $strConfigKey = null);



    /**
     * Get new or specified object instance event,
     * hydrated from specified configuration.
     *
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @param EventInterface $objEvent = null
     * @return null|EventInterface
     */
    public function getObjEvent(
        array $tabConfig,
        $strConfigKey = null,
        EventInterface $objEvent = null
    );
}