<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\event\observer\exception;

use liberty_code\event\observer\library\ConstObserver;



class EventNotFoundException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $strEventSelector
     */
	public function __construct($strEventSelector)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstObserver::EXCEPT_MSG_EVENT_NOT_FOUND,
            mb_strimwidth(strval($strEventSelector), 0, 50, "...")
        );
	}
}