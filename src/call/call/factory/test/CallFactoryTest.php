<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../../..';

// Load test
require_once($strRootAppPath . '/src/observer/test/ObserverTest.php');

// Use
use liberty_code\di\dependency\preference\model\Preference;
use liberty_code\event\call\call\factory\model\EventCallFactory;



// Init var
$objPref = new Preference(array(
    'source' => 'liberty_code\\event\\observer\\api\\ObserverInterface',
    'set' =>  ['type' => 'instance', 'value' => $objObserver],
    'option' => [
        'shared' => true
    ]
));
$objProvider->getObjDependencyCollection()->setDependency($objPref);

$objCallFactory = new EventCallFactory(
    $objObserver,
    $objCallFactory->getTabConfig(),
    $objCallFactory->getCallableBefore(),
    $objCallFactory->getCallableBefore(),
    $objCallFactory
);


