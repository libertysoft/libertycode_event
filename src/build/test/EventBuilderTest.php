<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load test
require_once($strRootAppPath . '/src/event/factory/test/EventFactoryTest.php');

// Use
use liberty_code\event\build\model\DefaultBuilder;

$objEventBuilder = new DefaultBuilder(
    $objEventFactory
);


