<?php
/**
 * Description :
 * This class allows to describe behavior of observer class.
 * Observer allows to execute callback function(s),
 * from specified event name or event key, using event collection.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\event\observer\api;

use liberty_code\event\event\api\EventCollectionInterface;



interface ObserverInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods check
	// ******************************************************************************

	/**
	 * Check if specified event key is found.
	 * 
	 * @param string $strKey
	 * @return boolean
	 */
	public function checkExists($strKey);



    /**
     * Check if specified event name is found,
     * on at least one event.
     *
     * @param string $strNm
     * @return boolean
     */
    public function checkNameExists($strNm);
	
	
	
	
	
	// Methods getters
	// ******************************************************************************
	
	/**
	 * Get event collection object.
	 *
	 * @return EventCollectionInterface
	 */
	public function getObjEventCollection();
	
	
	
	
	
	// Methods setters
	// ******************************************************************************
	
	/**
	 * Set event collection object.
	 * 
	 * @param EventCollectionInterface $objEventCollection
	 */
	public function setEventCollection(EventCollectionInterface $objEventCollection);
	
	
	
	
	
	// Methods execute
	// ******************************************************************************

    /**
     * Execute specified event name,
     * from specified array of arguments (for call destination),
     * and specified array of string elements (for call destination customization).
     *
     * @param string $strNm
     * @param array $tabCallArg = array()
     * @param array $tabStrCallElm = array()
     * @return mixed
     */
    public function execute(
        $strNm,
        array $tabCallArg = array(),
        array $tabStrCallElm = array()
    );



    /**
     * Execute specified event name, if exists.
     *
     * @param string $strNm
     * @param array $tabCallArg = array()
     * @param array $tabStrCallElm = array()
     * @return null|mixed
     */
    public function dispatch(
        $strNm,
        array $tabCallArg = array(),
        array $tabStrCallElm = array()
    );



	/**
	 * Execute specified event key,
     * from specified array of arguments (for call destination),
     * and specified array of string elements (for call destination customization).
	 * 
	 * @param string $strKey
	 * @param array $tabCallArg = array()
     * @param array $tabStrCallElm = array()
	 * @return mixed
	 */
	public function executeEvent(
	    $strKey,
        array $tabCallArg = array(),
        array $tabStrCallElm = array()
    );



    /**
     * Execute specified event key, if exists.
     *
     * @param string $strKey
     * @param array $tabCallArg = array()
     * @param array $tabStrCallElm = array()
     * @return null|mixed
     */
    public function dispatchEvent(
        $strKey,
        array $tabCallArg = array(),
        array $tabStrCallElm = array()
    );
}