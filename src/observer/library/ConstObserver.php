<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\event\observer\library;



class ConstObserver
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_EVENT_COLLECTION = 'objEventCollection';



    // Exception message constants
    const EXCEPT_MSG_COLLECTION_VALUE_INVALID_FORMAT = 'Following event collection "%1$s" invalid! It must be a event collection object.';
    const EXCEPT_MSG_EVENT_NOT_FOUND = 'No event found from following selector "%1s"!';
	const EXCEPT_MSG_CALLABLE_UNABLE_GET = 'Impossible to get callable from following event "%1$s"! Some event configuration elements are unsolved.';
}