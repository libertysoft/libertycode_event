<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load test
require_once($strRootAppPath . '/src/event/test/CallFactoryTest.php');

// Use
use liberty_code\event\event\api\EventInterface;
use liberty_code\event\event\model\DefaultEvent;
use liberty_code\event\event\model\DefaultEventCollection;



// Init var
$objEventCollection = new DefaultEventCollection($objCallFactory);



// Test add event
echo('Test add : <br />');

$objEvent1 = new DefaultEvent(
	array(
        'key' => 'event_1',
        'call' => [
            'class_path_pattern' => 'liberty_code\\event\\event\\test\\ControllerTest1:action'
        ]
    )
);

$objEvent2 = new DefaultEvent(
	array(
        'key' => 'event_2',
		'name' => [],
		'call' => [
			'type' => 'dependency',
			'dependency_key_pattern' => 'svc_1:action'
		]
    )
);

$objEvent3 = new DefaultEvent(
	array(
        'key' => 'event_3',
        'name' => ['event-a', 'event-b'],
		'call' => [
			'type' => 'file',
			'file_path_pattern' => '/src/event/test/FileControllerTest1.php',
            'file_path_format_require' => 7
		],
        'order' => 1
    )
);

$objEvent4 = new DefaultEvent(
    array(
        'key' => 'event_4',
        'name' => ['event-a', 'event-c'],
		'call' => [
			'type' => 'function',
			'file_path_pattern' => '/src/event/test/FileControllerTest2.php:runAction',
            'file_path_format_require' => true
		]
    )
);

$objEvent5 = new DefaultEvent(
	array(
        'key' => 'event_5',
        'name' => ['event-a', 'event-b'],
		'call' => [
            'type' => 'class',
            'class_path_pattern' => 'liberty_code\\event\\event\\test\\Controller%1$s',
            'method_name_pattern' => '%2$s'
		]
    )
);

$objEvent6 = new DefaultEvent(array(
    'key' => 'event_6',
    'name' => ['event-a', 'event-c'],
    'call' => [
        'type' => 'dependency',
        'dependency_key_pattern' => '%1$s',
        'method_pattern' => '%2$s'
    ],
    'order' => -1
));

$tabEvent = array(
    $objEvent1,
    $objEvent2,
    $objEvent3,
    $objEvent4,
    $objEvent5,
    $objEvent6
);
$objEventCollection->setTabEvent($tabEvent);

echo('<pre>');print_r($objEventCollection->beanGetTabData());echo('</pre>');

echo('<br /><br /><br />');



// Test check/get from key
$tabKey = array(
	'event_1' => [
        [],
	    ['Value 1 - Event 1', 'Value 2 - Event 1']
    ], // Ok
	'event_2' => [
        [],
	    ['Value 1 - Event 2', 'strAdd' => 'Value 2 - Event 2']
    ], // Ok
    'event_3' => [
        [],
        ['Value 1 - Event 3']
    ], // Ok
    'event_4' => [
        [],
        ['Value 1 - Event 4']
    ], // Ok
    'event_5' => [
        ['Test1', 'action'],
        ['Value 1 - Event 5']
    ], // Ok
    'event_6' => [
        ['svc_1', 'action'],
        ['strAdd2' => 'Value 1 - Event 6', 'strAdd' => 'Value 2 - Event 6']
    ], // Ok
	'test_1/test_2/test_3' => [
        [],
	    ['Value 1 - Event N']
    ], // Ko: Not found
	'test' => [
        [],
        []
    ], // Ko: Not found
	3 => [
        [],
        ['Value 1 - Event 4 by index']
    ], // Ok: Found index 3: event_4
	15 => [
        [],
        []
    ] // Ko: Not found
);

foreach($tabKey as $strKey => $tabInfo)
{
	echo('Test check, get key "'.$strKey.'": <br />');
	try{
        $tabStrCallElm = $tabInfo[0];
        $tabCallArg = $tabInfo[1];

        echo('Elements: <pre>');var_dump($tabStrCallElm);echo('</pre>');
        echo('Arguments: <pre>');var_dump($tabCallArg);echo('</pre>');

		$objEvent = $objEventCollection->getObjEvent($strKey);
		$boolEventExists = $objEventCollection->checkExists($strKey);

		echo('Check: <pre>');var_dump($boolEventExists);echo('</pre>');
		echo('Event: <pre>');var_dump($objEvent);echo('</pre>');
		
		if($boolEventExists)
		{
            $objCall = $objEvent->getObjCall();
			$callable = $objCall->getCallable($tabCallArg, $tabStrCallElm);
			
			echo('Event: key: <pre>');print_r($objEvent->getStrKey());echo('</pre>');
            echo('Event: name: <pre>');print_r($objEvent->getTabName());echo('</pre>');
			echo('Event: configuration: <pre>');var_dump($objEvent->getTabConfig());echo('</pre>');
			echo('Event: callable: <pre>');var_dump(is_callable($callable) ? $callable(): $callable);echo('</pre>');
            echo('Event: collection count: <pre>');print_r(count($objEvent->getObjEventCollection()->getTabKey()));echo('</pre>');
			echo('<br />');
		}
		else
		{
			echo('Event: not found<br />');
		}
	} catch(\Exception $e) {
		echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
		echo('<br />');
	}
	echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Test multi match
echo('Test multi match setting: <br />');

echo('Check: <pre>');var_dump($objEventCollection->checkMultiMatch());echo('</pre>');

$objEventCollection->setMultiMatch(false);
echo('Check after setting: <pre>');var_dump($objEventCollection->checkMultiMatch());echo('</pre>');

$objEventCollection->setMultiMatch(true);
echo('Check after setting: <pre>');var_dump($objEventCollection->checkMultiMatch());echo('</pre>');

echo('<br /><br /><br />');



// Test check/get from name
$tabName = array(
    'event_1', // Ok
    'test', // Ko: Not found
    'event-a', // Ok
    3, // Ko: Not found
    'event-b', // Ok
    ['test'], // Ko: Not found
    'event-c' // Ok
);

foreach($tabName as $strName)
{
    echo('Test check, get name: <br />');
    echo('<pre>');var_dump($strName);echo('</pre>');

    try{
        $tabEvent = $objEventCollection->getTabEvent($strName, true);
        $boolEventExists = $objEventCollection->checkNameExists($strName);

        echo('Check: <pre>');var_dump($boolEventExists);echo('</pre>');
        echo('Count event: <pre>');var_dump(count($tabEvent));echo('</pre>');

        if(count($tabEvent) > 0)
        {
            for($intCpt = 0; $intCpt < count($tabEvent); $intCpt++)
            {
                /** @var EventInterface $objEvent */
                $objEvent = $tabEvent[$intCpt];

                echo('Get['.$intCpt.']: event key: <pre>');print_r($objEvent->getStrKey());echo('</pre>');
                echo('<br />');
            }
        }
        else
        {
            echo('Get: not found<br />');
        }
    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Test remove event
echo('Test remove: <br />');

echo('Before removing: <pre>');print_r($objEventCollection->getTabKey());echo('</pre>');

$objEventCollection->removeEventAll();
echo('After removing: <pre>');print_r($objEventCollection->getTabKey());echo('</pre>');

echo('<br /><br /><br />');


