<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load test
require_once($strRootAppPath . '/src/observer/test/ObserverTest.php');



// Init var
$tabDataSrc = array(
    'event_1_not_care' => [
        'key' => 'event_1',
        'call' => [
            'class_path_pattern' => 'liberty_code\\event\\event\\test\\ControllerTest1:action',
            //'class_path_format_require' => true
        ]
    ],
    'event_2' => [
        'type' => 'default',
        'name' => [],
        'call' => [
            'type' => 'dependency',
            'dependency_key_pattern' => 'svc_1:action'
        ]
    ],
    'event_3' => [
        'name' => ['event-a', 'event-b'],
        'call' => [
            'type' => 'file',
            'file_path_pattern' => 'src/event/test/FileControllerTest1.php',
            'file_path_format_require' => 7
        ]
    ],
    [
        'type' => 'default',
        'name' => ['event-a', 'event-c'],
        'call' => [
            'type' => 'function',
            'file_path_pattern' => '/src/event/test/FileControllerTest2.php:runAction',
            'file_path_format_require' => true
        ]
    ],
    'event_5_not_care' => [
        'key' => 'event_5',
        'name' => ['event-a', 'event-b'],
        'call' => [
            'type' => 'class',
            'class_path_pattern' => 'liberty_code\\event\\event\\test\\Controller%1$s',
            'method_name_pattern' => '%2$s'
        ]
    ],
    'event_6' => [
        'type' => 'default',
        //'name' => ['event-a', 'event-c'],
        'call' => [
            'type' => 'class',
            'class_path_pattern' => 'liberty_code\\event\\event\\test\\Controller%1$s',
            'method_name_pattern' => '%2$s'
        ]
    ]
);

$objEventBuilder->setTabDataSrc($tabDataSrc);
$objEventBuilder->hydrateEventCollection($objEventCollection);



// Test check/get from key
$tabKey = array(
    'event_1' => [
        [],
        ['Value 1 - Event 1', 'Value 2 - Event 1']
    ], // Ok

    'event_2' => [
        [],
        ['strAdd2' => 'Value 1 - Event 2', 'strAdd' => 'Value 2 - Event 2']
    ], // Ok

    'event_3' => [
        [],
        ['Value 1 - Event 3']
    ], // Ok

    'event_4' => [
        [],
        ['Value 1 - Event 4']
    ], // Ko: Not found

    'event_5' => [
        ['Test1'],
        ['Value 1 - Event 5']
    ], // Ko: Callable unable get

    'event_6' => [
        ['Test1', 'action'],
        ['strAdd' => 'Value 1 - Event 6']
    ], // Ok

    'test_1/test_2/test_3' => [
        [],
        ['Value 1 - Event N']
    ], // Ko: Not found

    'test' => [
        [],
        []
    ], // Ko: Not found

    3 => [
        [],
        ['Value 1 - Event 4 by index']
    ], // Ok: Found index 3: event_4

    15 => [
        [],
        []
    ] // Ko: Not found
);

foreach($tabKey as $strKey => $tabInfo)
{
	echo('Test check, get key "'.$strKey.'": <br />');
	try{
        $tabStrCallElm = $tabInfo[0];
        $tabCallArg = $tabInfo[1];
		$callResult =  null;
        $dispatchResult = null;

		if($objObserver->checkNameExists($strKey))
		{
			echo('Check name: true<br>');

			$callResult = $objObserver->execute($strKey, $tabCallArg, $tabStrCallElm);
            $dispatchResult = $objObserver->dispatch($strKey, $tabCallArg, $tabStrCallElm);
		}
        if($objObserver->checkExists($strKey))
        {
            echo('Check key: true<br>');

            $callResult = $objObserver->executeEvent($strKey, $tabCallArg, $tabStrCallElm);
            $dispatchResult = $objObserver->dispatchEvent($strKey, $tabCallArg, $tabStrCallElm);
        }
		else
		{
			echo('Key not found<br />');
		}
		
		echo('Call: callable result: <pre>');var_dump($callResult);echo('</pre>');
        echo('Call: callable result (dispatch): <pre>');var_dump($dispatchResult);echo('</pre>');
	} catch(\Exception $e) {
		echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
		echo('<br />');
	}
	echo('<br /><br /><br />');
}

echo('<br /><br /><br />');


