<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load test
require_once($strRootAppPath . '/src/event/test/CallFactoryTest.php');

// Use
use liberty_code\event\event\factory\standard\model\StandardEventFactory;

$objEventFactory = new StandardEventFactory();


