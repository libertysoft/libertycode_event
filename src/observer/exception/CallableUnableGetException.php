<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\event\observer\exception;

use liberty_code\event\observer\library\ConstObserver;



class CallableUnableGetException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $strEventSelector
     */
	public function __construct($strEventSelector)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstObserver::EXCEPT_MSG_CALLABLE_UNABLE_GET,
            mb_strimwidth(strval($strEventSelector), 0, 50, "...")
        );
	}
}