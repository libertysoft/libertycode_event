<?php
/**
 * Description :
 * This class allows to define default event factory class.
 * Can be consider is base of all event factory type.
 *
 * Default event factory uses the following specified configuration, to get and hydrate route:
 * [
 *     type(optional): "string constant to determine event type",
 *
 *     ... specific event configuration
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\event\event\factory\model;

use liberty_code\di\factory\model\DefaultFactory;
use liberty_code\event\event\factory\api\EventFactoryInterface;

use liberty_code\library\reflection\library\ToolBoxReflection;
use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\event\event\api\EventInterface;
use liberty_code\event\event\api\EventCollectionInterface;
use liberty_code\event\event\exception\EventCollectionInvalidFormatException;
use liberty_code\event\event\factory\library\ConstEventFactory;
use liberty_code\event\event\factory\exception\FactoryInvalidFormatException;
use liberty_code\event\event\factory\exception\ConfigInvalidFormatException;



/**
 * @method null|EventCollectionInterface getObjEventCollection() Get event collection object.
 * @method void setObjEventCollection(null|EventCollectionInterface $objRouteCollection) Set event collection object.
 * @method null|EventFactoryInterface getObjFactory() Get parent factory object.
 * @method void setObjFactory(null|EventFactoryInterface $objFactory) Set parent factory object.
 */
abstract class DefaultEventFactory extends DefaultFactory implements EventFactoryInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param EventCollectionInterface $objEventCollection = null
     * @param EventFactoryInterface $objFactory = null
     */
    public function __construct(
        EventCollectionInterface $objEventCollection = null,
        EventFactoryInterface $objFactory = null,
        ProviderInterface $objProvider = null
    )
    {
        // Call parent constructor
        parent::__construct($objProvider);

        // Init event collection
        $this->setObjEventCollection($objEventCollection);

        // Init event factory
        $this->setObjFactory($objFactory);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * Overwrite hydrateDefault().
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstEventFactory::DATA_KEY_DEFAULT_EVENT_COLLECTION))
        {
            $this->__beanTabData[ConstEventFactory::DATA_KEY_DEFAULT_EVENT_COLLECTION] = null;
        }

        if(!$this->beanExists(ConstEventFactory::DATA_KEY_DEFAULT_FACTORY))
        {
            $this->__beanTabData[ConstEventFactory::DATA_KEY_DEFAULT_FACTORY] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstEventFactory::DATA_KEY_DEFAULT_EVENT_COLLECTION,
            ConstEventFactory::DATA_KEY_DEFAULT_FACTORY
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstEventFactory::DATA_KEY_DEFAULT_EVENT_COLLECTION:
                    EventCollectionInvalidFormatException::setCheck($value);
                    break;

                case ConstEventFactory::DATA_KEY_DEFAULT_FACTORY:
                    FactoryInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * Hydrate specified event.
     * Overwrite it to set specific call hydration.
     *
     * @param EventInterface $objEvent
     * @param array $tabConfigFormat
     * @throws ConfigInvalidFormatException
     */
    protected function hydrateEvent(EventInterface $objEvent, array $tabConfigFormat)
    {
        // Init formatted configuration
        if(array_key_exists(ConstEventFactory::TAB_CONFIG_KEY_TYPE, $tabConfigFormat))
        {
            unset($tabConfigFormat[ConstEventFactory::TAB_CONFIG_KEY_TYPE]);
        }

        // Hydrate event
        $objEventCollection = $this->getObjEventCollection();
        if(!is_null($objEventCollection))
        {
            $objEvent->setEventCollection($objEventCollection);
        }

        $objEvent->setConfig($tabConfigFormat);
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check if the specified formatted configuration is valid,
     * for the specified event object.
     *
     * @param EventInterface $objEvent
     * @param array $tabConfigFormat
     * @return boolean
     * @throws ConfigInvalidFormatException
     */
    protected function checkConfigIsValid(EventInterface $objEvent, array $tabConfigFormat)
    {
        // Init var
        $strEventClassPath = $this->getStrEventClassPathEngine($tabConfigFormat);
        $result = (
            (!is_null($strEventClassPath)) &&
            ($strEventClassPath == get_class($objEvent))
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get formatted configuration array.
     * Overwrite it to set specific feature.
     *
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @return array
     */
    protected function getTabConfigFormat(array $tabConfig, $strConfigKey = null)
    {
        return $tabConfig;
    }



    /**
     * Get string configured type,
     * from specified formatted configuration.
     *
     * @param array $tabConfigFormat
     * @return null|string
     * @throws ConfigInvalidFormatException
     */
    protected function getStrConfigType(array $tabConfigFormat)
    {
        // Check arguments
        ConfigInvalidFormatException::setCheck($tabConfigFormat);

        // Init var
        $result = null;

        // Get type, if found
        if(array_key_exists(ConstEventFactory::TAB_CONFIG_KEY_TYPE, $tabConfigFormat))
        {
            $result = $tabConfigFormat[ConstEventFactory::TAB_CONFIG_KEY_TYPE];
        }

        // Return result
        return $result;
    }



    /**
     * Get string class path of event,
     * from specified configured type.
     * Overwrite it to set specific feature.
     *
     * @param null|string $strConfigType
     * @return null|string
     */
    abstract protected function getStrEventClassPathFromType($strConfigType);



    /**
     * Get string class path of event engine,
     * from specified formatted configuration.
     *
     * @param array $tabConfigFormat
     * @return null|string
     * @throws ConfigInvalidFormatException
     */
    protected function getStrEventClassPathEngine(array $tabConfigFormat)
    {
        // Init var
        $strConfigType = $this->getStrConfigType($tabConfigFormat);
        $result = $this->getStrEventClassPathFromType($strConfigType);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getStrEventClassPath(array $tabConfig, $strConfigKey = null)
    {
        // Init var
        $tabConfigFormat = $this->getTabConfigFormat($tabConfig, $strConfigKey);
        $result = $this->getStrEventClassPathEngine($tabConfigFormat);

        // Get class path from parent factory, if required
        $objFactory = $this->getObjFactory();
        if(
            (!is_null($objFactory)) &&
            is_null($result)
        )
        {
            $result = $objFactory->getStrEventClassPath($tabConfig, $strConfigKey);
        }

        // Return result
        return $result;
    }



    /**
     * Get new object instance event,
     * from specified configured type.
     * Overwrite it to set specific feature.
     *
     * @param null|string $strConfigType
     * @return null|EventInterface
     */
    protected function getObjEventNew($strConfigType)
    {
        // Init var
        $strClassPath = $this->getStrEventClassPathFromType($strConfigType);

        // Init instance
        $result = $this->getObjInstance($strClassPath);
        if(is_null($result))
        {
            $result = ToolBoxReflection::getObjInstance($strClassPath);
        }

        // Return result
        return $result;
    }



    /**
     * Get object instance event engine.
     *
     * @param array $tabConfigFormat
     * @param EventInterface $objEvent = null
     * @return null|EventInterface
     * @throws ConfigInvalidFormatException
     */
    protected function getObjEventEngine(
        array $tabConfigFormat,
        EventInterface $objEvent = null
    )
    {
        // Init var
        $result = null;
        $strConfigType = $this->getStrConfigType($tabConfigFormat);
        $objEvent = (
            is_null($objEvent) ?
                $this->getObjEventNew($strConfigType) :
                $objEvent
        );

        // Get and hydrate event, if required
        if(
            (!is_null($objEvent)) &&
            $this->checkConfigIsValid($objEvent, $tabConfigFormat)
        )
        {
            $this->hydrateEvent($objEvent, $tabConfigFormat);
            $result = $objEvent;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getObjEvent(
        array $tabConfig,
        $strConfigKey = null,
        EventInterface $objEvent = null
    )
    {
        // Init var
        $tabConfigFormat = $this->getTabConfigFormat($tabConfig, $strConfigKey);
        $result = $this->getObjEventEngine($tabConfigFormat, $objEvent);

        // Get event from parent factory, if required
        $objFactory = $this->getObjFactory();
        if(
            (!is_null($objFactory)) &&
            is_null($result)
        )
        {
            $result = $objFactory->getObjEvent($tabConfig, $strConfigKey, $objEvent);
        }

        // Return result
        return $result;
    }



}