<?php
/**
 * Description :
 * This class allows to define default event class.
 * Can be consider is base of all event type.
 *
 * Default event uses the following specified configuration:
 * [
 *     key(optional: hash got if not found): "string event key",
 *
 *     name(optional: got [key] if not found): [
 *         "string event name 1",
 *         ...,
 *         "string event name N"
 *     ],
 *
 *     call(required):[
 *         Call builder destination configuration (@see CallFactoryInterface::getObjCall() )
 *     ],
 *
 *     order(optional: 0 got if not found): integer,
 *
 *     sort_compare_default(optional: got @see ConstEvent::SORT_COMPARE_EQUAL if not found):
 *         integer sort comparison analysis (@see EventInterface::getIntSortCompare() result format),
 *
 *     sort_compare_use_key(optional: got false if not found): true / false
 *         Use alphanumeric key sort comparison if true
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\event\event\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\event\event\api\EventInterface;

use liberty_code\call\call\factory\api\CallFactoryInterface;
use liberty_code\event\event\library\ConstEvent;
use liberty_code\event\event\api\EventCollectionInterface;
use liberty_code\event\event\exception\ConfigInvalidFormatException;
use liberty_code\event\event\exception\EventCollectionInvalidFormatException;



class DefaultEvent extends FixBean implements EventInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param array $tabConfig = null
     * @param EventCollectionInterface $objEventCollection = null
     */
    public function __construct(array $tabConfig = null, EventCollectionInterface $objEventCollection = null)
    {
        // Call parent constructor
        parent::__construct();

        // Init event collection if required
        if(!is_null($objEventCollection))
        {
            $this->setEventCollection($objEventCollection);
        }

        // Init configuration if required
        if(!is_null($tabConfig))
        {
            $this->setConfig($tabConfig);
        }
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstEvent::DATA_KEY_DEFAULT_EVENT_COLLECTION))
        {
            $this->__beanTabData[ConstEvent::DATA_KEY_DEFAULT_EVENT_COLLECTION] = null;
        }

        if(!$this->beanExists(ConstEvent::DATA_KEY_DEFAULT_CONFIG))
        {
            $this->__beanTabData[ConstEvent::DATA_KEY_DEFAULT_CONFIG] = array();
        }
    }
	



	
    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstEvent::DATA_KEY_DEFAULT_EVENT_COLLECTION,
            ConstEvent::DATA_KEY_DEFAULT_CONFIG
        );
        $result = in_array($key, $tabKey);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstEvent::DATA_KEY_DEFAULT_EVENT_COLLECTION:
                    EventCollectionInvalidFormatException::setCheck($value);
                    break;

                case ConstEvent::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidRemove($key, &$error = null)
    {
        // Return result
        return false;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check if sort comparison by key option is selected.
     *
     * @return boolean
     */
    protected function checkSortCompareUseKey()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            array_key_exists(ConstEvent::TAB_CONFIG_KEY_SORT_COMPARE_USE_KEY, $tabConfig) ?
                (intval($tabConfig[ConstEvent::TAB_CONFIG_KEY_SORT_COMPARE_USE_KEY]) != 0) :
                false
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

	/**
     * @inheritdoc
     */
    public function getObjEventCollection()
    {
        // Return result
        return $this->beanGet(ConstEvent::DATA_KEY_DEFAULT_EVENT_COLLECTION);
    }



    /**
     * Get call factory object.
     *
     * @return null|CallFactoryInterface
     */
    protected function getObjCallFactory()
    {
        // Init var
        $result = null;
        $objEventCollection = $this->getObjEventCollection();

        // Get call factory if found
        if(!is_null($objEventCollection))
        {
            $result = $objEventCollection->getObjCallFactory();
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getStrKey()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            array_key_exists(ConstEvent::TAB_CONFIG_KEY_KEY, $tabConfig) ?
                $tabConfig[ConstEvent::TAB_CONFIG_KEY_KEY] :
                static::getStrHash($this)
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getTabName()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            array_key_exists(ConstEvent::TAB_CONFIG_KEY_NAME, $tabConfig) ?
                $tabConfig[ConstEvent::TAB_CONFIG_KEY_NAME] :
                array($this->getStrKey())
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getTabConfig()
    {
        // Return result
        return $this->beanGet(ConstEvent::DATA_KEY_DEFAULT_CONFIG);
    }



    /**
     * Get integer order,
     * from configuration.
     *
     * @return integer
     */
    public function getIntOrder()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            array_key_exists(ConstEvent::TAB_CONFIG_KEY_ORDER, $tabConfig) ?
                $tabConfig[ConstEvent::TAB_CONFIG_KEY_ORDER] :
                0
        );

        // Return result
        return $result;
    }



    /**
     * Get default sort comparison analysis,
     * from configuration.
     *
     * @return integer
     */
    protected function getIntSortCompareDefault()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            array_key_exists(ConstEvent::TAB_CONFIG_KEY_SORT_COMPARE_DEFAULT, $tabConfig) ?
                $tabConfig[ConstEvent::TAB_CONFIG_KEY_SORT_COMPARE_DEFAULT] :
                ConstEvent::SORT_COMPARE_EQUAL
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getObjCall()
    {
        // Init var
        $result = null;
        $objCallFactory = $this->getObjCallFactory();
        $tabConfig = $this->getTabConfig();

        // Get call, if possible
        if(
            array_key_exists(ConstEvent::TAB_CONFIG_KEY_CALL, $tabConfig) &&
            (!is_null($objCallFactory))
        )
        {
            $tabCallConfig = $tabConfig[ConstEvent::TAB_CONFIG_KEY_CALL];
            $result = $objCallFactory->getObjCall($tabCallConfig);
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getIntSortCompare(EventInterface $objEvent)
    {
        // Init var
        $result = ConstEvent::SORT_COMPARE_EQUAL;

        // Get order comparison, if required
        if($objEvent instanceof DefaultEvent)
        {
            $intOrder = $this->getIntOrder();
            $intEventOrder = $objEvent->getIntOrder();
            $result = (
                ($intOrder > $intEventOrder) ?
                    ConstEvent::SORT_COMPARE_GREATER :
                    (
                        ($intOrder < $intEventOrder) ?
                            ConstEvent::SORT_COMPARE_LESS :
                            ConstEvent::SORT_COMPARE_EQUAL
                    )
            );
        }

        // Get alphanumeric key comparison, if required
        if(
            ($result == ConstEvent::SORT_COMPARE_EQUAL) &&
            $this->checkSortCompareUseKey()
        )
        {
            $strKey = $this->getStrKey();
            $strEventKey = $objEvent->getStrKey();
            $result = (
                ($strKey > $strEventKey) ?
                    ConstEvent::SORT_COMPARE_GREATER :
                    (
                        ($strKey < $strEventKey) ?
                            ConstEvent::SORT_COMPARE_LESS :
                            ConstEvent::SORT_COMPARE_EQUAL
                    )
            );
        }

        // Get default comparison, if required
        $result = (
            ($result == ConstEvent::SORT_COMPARE_EQUAL) ?
                $this->getIntSortCompareDefault() :
                $result
        );

        // Return result
        return $result;
    }





    // Methods setters
    // ******************************************************************************
	
	/**
	 * @inheritdoc
	 */
	public function setEventCollection(EventCollectionInterface $objEventCollection = null)
	{
        $this->beanSet(ConstEvent::DATA_KEY_DEFAULT_EVENT_COLLECTION, $objEventCollection);
	}



    /**
     * @inheritdoc
     */
    public function setConfig(array $tabConfig)
    {
        $this->beanSet(ConstEvent::DATA_KEY_DEFAULT_CONFIG, $tabConfig);
    }





    // Methods statics
    // ******************************************************************************

    /**
     * Get string hash.
     *
     * @param $objEvent
     * @return string
     */
    public static function getStrHash(self $objEvent)
    {
        // Return result
        return ConstEvent::CONF_DEFAULT_EVENT_HASH_PREFIX . spl_object_hash($objEvent);
    }



}