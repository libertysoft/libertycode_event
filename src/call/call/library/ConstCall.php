<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\event\call\call\library;



class ConstCall
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_OBSERVER = 'objObserver';
	
	
	
    // Exception message constants
    const EXCEPT_MSG_OBSERVER_INVALID_FORMAT = 'Following observer "%1$s" invalid! It must be an observer object.';
}