<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\event\call\call\exception;

use liberty_code\event\observer\api\ObserverInterface;
use liberty_code\event\call\call\library\ConstCall;



class ObserverInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $observer
     */
	public function __construct($observer)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstCall::EXCEPT_MSG_OBSERVER_INVALID_FORMAT,
            mb_strimwidth(strval($observer), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified event observer has valid format
	 * 
     * @param mixed $observer
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($observer)
    {
		// Init var
		$result = (
			(is_null($observer)) ||
			($observer instanceof ObserverInterface)
		);
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($observer);
		}
		
		// Return result
		return $result;
    }
	
	
	
}