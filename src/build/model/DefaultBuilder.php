<?php
/**
 * Description :
 * This class allows to define default builder class.
 * Default builder allows to populate event collection,
 * from a specified array of source data.
 *
 * Default builder uses the following specified source data, to hydrate event collection:
 * [
 *     // Event configuration 1
 *     Event factory configuration (@see EventFactoryInterface ),
 *
 *     ...,
 *
 *     // Event configuration N
 *     ...
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\event\build\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\event\build\api\BuilderInterface;

use liberty_code\event\event\api\EventCollectionInterface;
use liberty_code\event\event\factory\api\EventFactoryInterface;
use liberty_code\event\build\library\ConstBuilder;
use liberty_code\event\build\exception\FactoryInvalidFormatException;
use liberty_code\event\build\exception\DataSrcInvalidFormatException;



/**
 * @method null|EventFactoryInterface getObjFactory() Get event factory object.
 * @method void setObjFactory(null|EventFactoryInterface $objFactory) Set event factory object.
 * @method array getTabDataSrc() get data source array.
 * @method void setTabDataSrc(array $tabDataSrc) Set data source array.
 */
class DefaultBuilder extends FixBean implements BuilderInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param EventFactoryInterface $objFactory
     * @param array $tabDataSrc = array()
     */
    public function __construct(
        EventFactoryInterface $objFactory,
        array $tabDataSrc = array()
    )
    {
        // Call parent constructor
        parent::__construct();

        // Init route factory
        $this->setObjFactory($objFactory);

        // Init data source
        $this->setTabDataSrc($tabDataSrc);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstBuilder::DATA_KEY_DEFAULT_FACTORY))
        {
            $this->beanAdd(ConstBuilder::DATA_KEY_DEFAULT_FACTORY, null);
        }

        if(!$this->beanExists(ConstBuilder::DATA_KEY_DEFAULT_DATA_SRC))
        {
            $this->beanAdd(ConstBuilder::DATA_KEY_DEFAULT_DATA_SRC, array());
        }
    }



    /**
     * @inheritdoc
     * @throws DataSrcInvalidFormatException
     */
    public function hydrateEventCollection(EventCollectionInterface $objEventCollection, $boolClear = true)
    {
        // Init var
        $boolClear = (is_bool($boolClear) ? $boolClear : true);
        $objFactory = $this->getObjFactory();
        $tabDataSrc = $this->getTabDataSrc();

        // Run each data source
        $tabEvent = array();
        foreach($tabDataSrc as $key => $tabConfig)
        {
            // Get new event
            $key = (is_string($key) ? $key : null);
            $objEvent = $objFactory->getObjEvent($tabConfig, $key);

            // Register event, if found
            if(!is_null($objEvent))
            {
                $tabEvent[] = $objEvent;
            }
            // Throw exception if event not found, from data source
            else
            {
                throw new DataSrcInvalidFormatException(serialize($tabDataSrc));
            }

        }

        // Clear events from collection, if required
        if($boolClear)
        {
            $objEventCollection->removeEventAll();
        }

        // Register events on collection
        $objEventCollection->setTabEvent($tabEvent);
    }





	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function beanCheckValidKey($key, &$error = null)
	{
		// Init var
		$tabKey = array(
            ConstBuilder::DATA_KEY_DEFAULT_FACTORY,
			ConstBuilder::DATA_KEY_DEFAULT_DATA_SRC
		);
		$result = in_array($key, $tabKey);

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidValue($key, $value, &$error = null)
	{
		// Init var
		$result = true;

		// Validation
		try
		{
			switch($key)
			{
                case ConstBuilder::DATA_KEY_DEFAULT_FACTORY:
                    FactoryInvalidFormatException::setCheck($value);
                    break;

				case ConstBuilder::DATA_KEY_DEFAULT_DATA_SRC:
                    DataSrcInvalidFormatException::setCheck($value);
					break;
			}
		}
		catch(\Exception $e)
		{
			$result = false;
			$error = $e;
		}

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidRemove($key, &$error = null)
	{
		// Return result
		return false;
	}
	
	
	
}