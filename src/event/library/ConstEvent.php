<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\event\event\library;



class ConstEvent
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_EVENT_COLLECTION = 'objEventCollection';
    const DATA_KEY_DEFAULT_CONFIG = 'tabConfig';



    // Configuration
    const CONF_DEFAULT_EVENT_HASH_PREFIX = 'default_event_';

    // Event configuration
    const TAB_CONFIG_KEY_KEY = 'key';
    const TAB_CONFIG_KEY_NAME = 'name';
    const TAB_CONFIG_KEY_CALL = 'call';
    const TAB_CONFIG_KEY_ORDER = 'order';
    const TAB_CONFIG_KEY_SORT_COMPARE_DEFAULT = 'sort_compare_default';
    const TAB_CONFIG_KEY_SORT_COMPARE_USE_KEY = 'sort_compare_use_key';

    // Sort comparison value configuration
    const SORT_COMPARE_LESS = -1;
    const SORT_COMPARE_EQUAL = 0;
    const SORT_COMPARE_GREATER = 1;

    // Collection configuration
    const TAB_COLLECTION_CONFIG_KEY_MULTI_MATCH = 'multi_match';



    // Exception message constants
    const EXCEPT_MSG_EVENT_COLLECTION_INVALID_FORMAT = 'Following event collection "%1$s" invalid! It must be an event collection object.';
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following configuration "%1$s" invalid! 
        The configuration must be an array and following the default event configuration standard.';
    const EXCEPT_MSG_COLLECTION_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the default event collection configuration standard.';
    const EXCEPT_MSG_COLLECTION_KEY_INVALID_FORMAT = 'Key invalid! The key "%1$s" must be a valid string in collection.';
    const EXCEPT_MSG_COLLECTION_VALUE_INVALID_FORMAT = 'Value invalid! The value "%1$s" must be an event object in collection.';





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods statics getters
    // ******************************************************************************

    /**
     * Get index array of sort comparison values.
     *
     * @return array
     */
    static public function getTabSortCompare()
    {
        // Init var
        $result = array(
            self::SORT_COMPARE_LESS,
            self::SORT_COMPARE_EQUAL,
            self::SORT_COMPARE_GREATER
        );

        // Return result
        return $result;
    }



}