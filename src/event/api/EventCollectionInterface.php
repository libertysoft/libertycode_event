<?php
/**
 * Description :
 * This class allows to describe behavior of events collection class.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\event\event\api;

use liberty_code\call\call\factory\api\CallFactoryInterface;
use liberty_code\event\event\api\EventInterface;



interface EventCollectionInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods check
	// ******************************************************************************

    /**
     * Check if multi match option is selected.
     * This option allows to specify if event name search include all events found,
     * false means include only first event found.
     *
     * @return boolean
     */
    public function checkMultiMatch();



	/**
	 * Check if specified event key is found.
	 * 
	 * @param string $strKey
	 * @return boolean
	 */
	public function checkExists($strKey);



    /**
     * Check if specified event name is found,
     * on at least one event.
     *
     * @param string $strNm
     * @return boolean
     */
    public function checkNameExists($strNm);
	
	
	
	
	
	// Methods getters
	// ******************************************************************************

    /**
     * Get call factory object.
     *
     * @return null|CallFactoryInterface
     */
    public function getObjCallFactory();



    /**
     * Get index array of keys.
     *
     * @param null|boolean $sortAsc = null
     * @return array
     */
    public function getTabKey($sortAsc = null);


	
	/**
	 * Get event,
     * from specified key.
	 * 
	 * @param string $strKey
	 * @return null|EventInterface
	 */
	public function getObjEvent($strKey);



    /**
     * Get index array of events,
     * from specified event name.
     *
     * @param string $strNm
     * @param null|boolean $sortAsc = null
     * @return EventInterface[]
     */
    public function getTabEvent($strNm, $sortAsc = null);

	
	
	
	
	// Methods setters
	// ******************************************************************************

    /**
     * Set call factory object.
     *
     * @param CallFactoryInterface $objCallFactory
     */
    public function setCallFactory(CallFactoryInterface $objCallFactory);



    /**
     * Set multi match option.
     * Option description: @see checkMultiMatch() .
     *
     * @param boolean $boolMultiMatch
     */
    public function setMultiMatch($boolMultiMatch);



	/**
	 * Set event and return its key.
	 * 
	 * @param EventInterface $objEvent
	 * @return string
     */
	public function setEvent(EventInterface $objEvent);



    /**
     * Set list of events (index array or collection) and
     * return its list of keys (index array).
     *
     * @param array|EventCollectionInterface $tabEvent
     * @return array
     */
    public function setTabEvent($tabEvent);



    /**
     * Remove event and return its instance.
     *
     * @param string $strKey
     * @return EventInterface
     */
    public function removeEvent($strKey);



    /**
     * Remove all events.
     */
    public function removeEventAll();
}