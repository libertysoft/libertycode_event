<?php
/**
 * Description :
 * This class allows to define default events collection class.
 * key: event key => event.
 *
 * Default event collection uses the following specified configuration:
 * [
 *     multi_match(optional: got true if multi_match not found): true / false
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\event\event\model;

use liberty_code\library\bean\model\DefaultBean;
use liberty_code\event\event\api\EventCollectionInterface;

use liberty_code\library\bean\library\ConstBean;
use liberty_code\call\call\factory\api\CallFactoryInterface;
use liberty_code\event\event\library\ConstEvent;
use liberty_code\event\event\api\EventInterface;
use liberty_code\event\event\exception\CollectionConfigInvalidFormatException;
use liberty_code\event\event\exception\CollectionKeyInvalidFormatException;
use liberty_code\event\event\exception\CollectionValueInvalidFormatException;



class DefaultEventCollection extends DefaultBean implements EventCollectionInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



    /** @var CallFactoryInterface */
    protected $objCallFactory;



    /** @var array */
    protected $tabConfig;





	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param CallFactoryInterface $objCallFactory = null
     * @param array $tabConfig = null
     */
    public function __construct(
        CallFactoryInterface $objCallFactory = null,
        array $tabConfig = null,
        array $tabData = array())
    {
        // Init var
        $this->objCallFactory = null;
        $this->tabConfig = array();

        // Call parent constructor
        parent::__construct($tabData);

        // Init call factory if required
        if(!is_null($objCallFactory))
        {
            $this->setCallFactory($objCallFactory);
        }

        // Init configuration, if required
        if(!is_null($tabConfig))
        {
            $this->setConfig($tabConfig);
        }
    }

	
	
	
	
	// Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            // Check value argument
            CollectionValueInvalidFormatException::setCheck($value);

            // Check key argument
            /** @var EventInterface $value */
            if(
                (!is_string($key)) ||
                ($key != $value->getStrKey())
            )
            {
                throw new CollectionKeyInvalidFormatException($key);
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function checkMultiMatch()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            (!array_key_exists(ConstEvent::TAB_COLLECTION_CONFIG_KEY_MULTI_MATCH, $tabConfig)) ||
            (intval($tabConfig[ConstEvent::TAB_COLLECTION_CONFIG_KEY_MULTI_MATCH]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function checkExists($strKey)
    {
        // Return result
        return (!is_null($this->getObjEvent($strKey)));
    }



    /**
     * @inheritdoc
     */
    public function checkNameExists($strNm)
    {
        // Return result
        return (count($this->getTabEvent($strNm)) > 0);
    }





	// Methods getters
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getObjCallFactory()
    {
        // Return result
        return $this->objCallFactory;
    }



    /**
     * Get configuration array.
     *
     * @return array
     */
    public function getTabConfig()
    {
        // Return result
        return $this->tabConfig;
    }



    /**
     * @inheritdoc
     */
    public function getTabKey($sortAsc = null)
    {
        // Init var
        $result = $this->beanGetTabData(ConstBean::OPTION_TABLE_DATA_KEY);
        $boolSortAsc = (is_bool($sortAsc) ? $sortAsc : null);

        // Sort, if required
        if(is_bool($boolSortAsc))
        {
            // Sort
            usort(
                $result,
                function($strKey1, $strKey2)
                {
                    $objEvent1 = $this->getObjEvent($strKey1);
                    $objEvent2 = $this->getObjEvent($strKey2);
                    $result = $objEvent1->getIntSortCompare($objEvent2);

                    return $result;
                }
            );

            // Get descending sort, if required
            if(!$boolSortAsc)
            {
                krsort($result);
                $result = array_values($result);
            }
        }

        // Return result
        return $result;
    }


	
	/**
	 * @inheritdoc
	 */
	public function getObjEvent($strKey)
	{
        // Init var
        $result = null;

        // Try to get event object if found
        try
        {
            if($this->beanDataExists($strKey))
            {
                $result = $this->beanGetData($strKey);;
            }
        }
        catch(\Exception $e)
        {
        }

        // Return result
        return $result;
	}



    /**
     * @inheritdoc
     */
    public function getTabEvent($strNm, $sortAsc = null)
    {
        // Init var
        $result = array();
        $boolMultiMatch = $this->checkMultiMatch();

        // Run each event
        $tabKey = $this->getTabKey($sortAsc);
        for($intCpt = 0; ($intCpt < count($tabKey)) && ($boolMultiMatch || (count($result) == 0)); $intCpt++)
        {
            // Get event
            $strKey = $tabKey[$intCpt];
            $objEvent = $this->getObjEvent($strKey);

            // Register event, if required
            if(in_array($strNm, $objEvent->getTabName()))
            {
                $result[] = $objEvent;
            }
        }

        // Return result
        return $result;
    }
	
	
	
	
	
	// Methods setters
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    public function setCallFactory(CallFactoryInterface $objCallFactory)
    {
        // Set data
        $this->objCallFactory = $objCallFactory;
    }



    /**
     * Set configuration array.
     *
     * @param array $tabConfig
     * @throws CollectionConfigInvalidFormatException
     */
    public function setConfig(array $tabConfig)
    {
        // Set check argument
        CollectionConfigInvalidFormatException::setCheck($tabConfig);

        $this->tabConfig = $tabConfig;
    }



    /**
     * @inheritdoc
     * @throws CollectionConfigInvalidFormatException
     */
    public function setMultiMatch($boolMultiMatch)
    {
        // Init var
        $tabConfig = $this->getTabConfig();

        // Set force access option
        $tabConfig[ConstEvent::TAB_COLLECTION_CONFIG_KEY_MULTI_MATCH] = $boolMultiMatch;

        // Set configuration
        $this->setConfig($tabConfig);
    }



	/**
	 * @inheritdoc
     */
	public function setEvent(EventInterface $objEvent)
	{
        // Init var
        $strKey = $objEvent->getStrKey();

        // Register instance
        $this->beanPutData($strKey, $objEvent);

        // return result
        return $strKey;
	}



    /**
     * @inheritdoc
     * @throws CollectionKeyInvalidFormatException
     * @throws CollectionValueInvalidFormatException
     */
    protected function beanSet($key, $val, $boolTranslate = false)
    {
        // Call parent method
        parent::beanSet($key, $val, $boolTranslate);

        // Register event collection on event object
        /** @var EventInterface $val */
        $val->setEventCollection($this);
    }



    /**
     * @inheritdoc
     */
    public function setTabEvent($tabEvent)
    {
        // Init var
        $result = array();

        // Case index array of events
        if(is_array($tabEvent))
        {
            // Run all events and for each, try to set
            foreach($tabEvent as $event)
            {
                $strKey = $this->setEvent($event);
                $result[] = $strKey;
            }
        }
        // Case collection of events
        else if($tabEvent instanceof EventCollectionInterface)
        {
            // Run all events and for each, try to set
            foreach($tabEvent->getTabKey() as $strKey)
            {
                $objEvent = $tabEvent->getObjEvent($strKey);
                $strKey = $this->setEvent($objEvent);
                $result[] = $strKey;
            }
        }

        // return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function removeEvent($strKey)
    {
        // Init var
        $result = $this->getObjEvent($strKey);

        // Remove route
        $this->beanRemoveData($strKey);

        // return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function removeEventAll()
    {
        // Ini var
        $tabKey = $this->getTabKey();

        foreach($tabKey as $strKey)
        {
            $this->removeEvent($strKey);
        }
    }



}