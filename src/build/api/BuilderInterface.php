<?php
/**
 * Description :
 * This class allows to describe behavior of builder class.
 * Builder allows to populate specified event collection instance, with events.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\event\build\api;

use liberty_code\event\event\api\EventCollectionInterface;



interface BuilderInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods initialize
    // ******************************************************************************

    /**
     * Hydrate specified event collection.
     *
     * @param EventCollectionInterface $objEventCollection
     * @param boolean $boolClear = true
     */
    public function hydrateEventCollection(EventCollectionInterface $objEventCollection, $boolClear = true);
}