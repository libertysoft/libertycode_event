<?php
/**
 * Description :
 * Event allows to get a call and it's callback function,
 * from specified string key or string name.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\event\event\api;

use liberty_code\call\call\api\CallInterface;
use liberty_code\event\event\library\ConstEvent;
use liberty_code\event\event\api\EventCollectionInterface;



interface EventInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods getters
	// ******************************************************************************
	
	/**
	 * Get event collection object.
	 *
	 * @return null|EventCollectionInterface
	 */
	public function getObjEventCollection();
	
	
	
	/**
	 * Get string key (considered as event id).
	 *
	 * @return string
	 */
	public function getStrKey();



    /**
     * Get index array of string name.
     *
     * @return array
     */
    public function getTabName();



	/**
     * Get configuration array.
     *
     * @return array
     */
    public function getTabConfig();



    /**
     * Get call object.
     *
     * @return null|CallInterface
     */
    public function getObjCall();



    /**
     * Get sort comparison analysis,
     * from the specified event.
     * Result format:
     * @see ConstEvent::SORT_COMPARE_LESS if this event less than specified event.
     * @see ConstEvent::SORT_COMPARE_EQUAL if this event equal specified event.
     * @see ConstEvent::SORT_COMPARE_GREATER if this event greater specified event.
     *
     * @param EventInterface $objEvent
     * @return integer
     */
    public function getIntSortCompare(EventInterface $objEvent);



	
	
	// Methods setters
	// ******************************************************************************
	
	/**
	 * Set event collection object.
	 * 
	 * @param EventCollectionInterface $objEventCollection = null
	 */
	public function setEventCollection(EventCollectionInterface $objEventCollection = null);



    /**
     * Set configuration array.
     *
     * @param array $tabConfig
     */
    public function setConfig(array $tabConfig);
}