<?php
/**
 * Description :
 * This class allows to define default observer class.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\event\observer\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\event\observer\api\ObserverInterface;

use liberty_code\event\event\api\EventInterface;
use liberty_code\event\event\api\EventCollectionInterface;
use liberty_code\event\observer\library\ConstObserver;
use liberty_code\event\observer\exception\EventCollectionInvalidFormatException;
use liberty_code\event\observer\exception\EventNotFoundException;
use liberty_code\event\observer\exception\CallableUnableGetException;



class DefaultObserver extends FixBean implements ObserverInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param EventCollectionInterface $objEventCollection
     */
    public function __construct(EventCollectionInterface $objEventCollection)
    {
        // Call parent constructor
        parent::__construct();

        // Init event collection
        $this->setEventCollection($objEventCollection);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstObserver::DATA_KEY_DEFAULT_EVENT_COLLECTION))
        {
            $this->__beanTabData[ConstObserver::DATA_KEY_DEFAULT_EVENT_COLLECTION] = null;
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstObserver::DATA_KEY_DEFAULT_EVENT_COLLECTION
        );
        $result = in_array($key, $tabKey);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstObserver::DATA_KEY_DEFAULT_EVENT_COLLECTION:
                    EventCollectionInvalidFormatException::setCheck($value);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidRemove($key, &$error = null)
    {
        // Return result
        return false;
    }

	
	
	
	
	// Methods check
	// ******************************************************************************
	
	/**
	 * @inheritdoc
	 */
	public function checkExists($strKey)
	{
		// Return result
		return $this->getObjEventCollection()->checkExists($strKey);
	}



    /**
     * @inheritdoc
     */
    public function checkNameExists($strNm)
    {
        // Return result
        return $this->getObjEventCollection()->checkNameExists($strNm);
    }

	
	
	
	
    // Methods getters
    // ******************************************************************************

	/**
     * @inheritdoc
     */
    public function getObjEventCollection()
    {
        // Return result
        return $this->beanGet(ConstObserver::DATA_KEY_DEFAULT_EVENT_COLLECTION);
    }


	


    // Methods setters
    // ******************************************************************************
	
	/**
	 * @inheritdoc
	 */
	public function setEventCollection(EventCollectionInterface $objEventCollection)
	{
		$this->beanSet(ConstObserver::DATA_KEY_DEFAULT_EVENT_COLLECTION, $objEventCollection);
	}


	

	
	// Methods execute
    // ******************************************************************************

    /**
     * @inheritdoc
     * @return null|mixed
     * @throws EventNotFoundException
     * @throws CallableUnableGetException
     */
    public function execute(
        $strNm,
        array $tabCallArg = array(),
        array $tabStrCallElm = array()
    )
    {
        // Init var
        $result = null;

        // Check event found
        if($this->checkNameExists($strNm))
        {
            // Get info
            $tabEvent = $this->getObjEventCollection()->getTabEvent($strNm, true);

            // Run each event
            foreach($tabEvent as $objEvent)
            {
                /** @var EventInterface $objEvent */

                // Execute and register callable result
                $result[] = $this->executeEvent($objEvent->getStrKey(), $tabCallArg, $tabStrCallElm);
            }
        }
        else
        {
            throw new EventNotFoundException($strNm);
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @return null|mixed
     * @throws CallableUnableGetException
     */
    public function dispatch(
        $strNm,
        array $tabCallArg = array(),
        array $tabStrCallElm = array()
    )
    {
        // Init var
        $result = null;

        // Check event found
        if($this->checkNameExists($strNm))
        {
            // Execute
            $result = $this->execute($strNm, $tabCallArg, $tabStrCallElm);
        }

        // Return result
        return $result;
    }



	/**
	 * @inheritdoc
	 * @return null|mixed
	 * @throws EventNotFoundException
	 * @throws CallableUnableGetException
	 */
    public function executeEvent(
        $strKey,
        array $tabCallArg = array(),
        array $tabStrCallElm = array()
    )
	{
		// Init var
		$result = null;
		
		// Check event found
		if($this->checkExists($strKey))
		{
			// Get info
			$objEvent = $this->getObjEventCollection()->getObjEvent($strKey);
            $objCall = $objEvent->getObjCall();
            $callable = (
                (!is_null($objCall)) ?
                    $objCall->getCallable($tabCallArg, $tabStrCallElm) :
                    null
            );
			
			// Check callable valid
			if(!is_null($callable))
			{
				// Execute and register callable result
				$result = $callable();
			}
			else
			{
				throw new CallableUnableGetException($strKey);
			}
		}
		else
		{
			throw new EventNotFoundException($strKey);
		}
		
		// Return result
		return $result;
	}



    /**
     * @inheritdoc
     * @return null|mixed
     * @throws CallableUnableGetException
     */
    public function dispatchEvent(
        $strKey,
        array $tabCallArg = array(),
        array $tabStrCallElm = array()
    )
    {
        // Init var
        $result = null;

        // Check event found
        if($this->checkExists($strKey))
        {
            // Execute
            $result = $this->executeEvent($strKey, $tabCallArg, $tabStrCallElm);
        }

        // Return result
        return $result;
    }



}