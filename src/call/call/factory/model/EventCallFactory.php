<?php
/**
 * Description :
 * This class allows to define event call factory class.
 * Event call factory allows to provide and hydrate event call instance.
 *
 * Event call factory uses the following specified configuration, to hydrate call:
 * [
 *     Event call configuration (@see EventCall )
 * ]
 *
 * Event call factory uses the following specified destination configuration, to get and hydrate call:
 * [
 *     type(optional): "event",
 *     Event call destination configuration (@see EventCall )
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\event\call\call\factory\model;

use liberty_code\call\call\factory\model\DefaultCallFactory;

use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\call\call\api\CallInterface;
use liberty_code\call\call\factory\api\CallFactoryInterface;
use liberty_code\event\observer\api\ObserverInterface;
use liberty_code\event\call\call\exception\ObserverInvalidFormatException;
use liberty_code\event\call\call\event\model\EventCall;
use liberty_code\event\call\call\factory\library\ConstEventCallFactory;



/**
 * @method ObserverInterface getObjObserver() Get event observer object.
 * @method void setObjObserver(ObserverInterface $objObserver) Set event observer object.
 */
class EventCallFactory extends DefaultCallFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param ObserverInterface $objObserver
     */
    public function __construct(
        ObserverInterface $objObserver,
        array $tabConfig = null,
        $callableBefore = null,
        $callableAfter = null,
        CallFactoryInterface $objFactory = null,
        ProviderInterface $objProvider = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $tabConfig,
            $callableBefore,
            $callableAfter,
            $objFactory,
            $objProvider
        );

        // Init router
        $this->setObjObserver($objObserver);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstEventCallFactory::DATA_KEY_DEFAULT_OBSERVER))
        {
            $this->__beanTabData[ConstEventCallFactory::DATA_KEY_DEFAULT_OBSERVER] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }



    /**
     * @inheritdoc
     */
    protected function hydrateCall(CallInterface $objCall, array $tabCallConfig)
    {
        // Hydrate DI call, if required
        if($objCall instanceof EventCall)
        {
            $objCall->setObjObserver($this->getObjObserver());
        }

        // Return result
        return parent::hydrateCall($objCall, $tabCallConfig);
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstEventCallFactory::DATA_KEY_DEFAULT_OBSERVER
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstEventCallFactory::DATA_KEY_DEFAULT_OBSERVER:
                    ObserverInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





	// Methods getters
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrCallClassPathFromType($strCallConfigType)
    {
        // Init var
        $result = null;

        // Get class path of call, from type
        switch($strCallConfigType)
        {
            case null:
            case ConstEventCallFactory::CALL_CONFIG_TYPE_EVENT:
                $result = EventCall::class;
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getObjCallNew($strCallConfigType)
    {
        // Init var
        $result = null;
        $objObserver = $this->getObjObserver();

        // Get class path of call, from type
        switch($strCallConfigType)
        {
            case null:
            case ConstEventCallFactory::CALL_CONFIG_TYPE_EVENT:
                $result = new EventCall($objObserver);
                break;
        }

        // Return result
        return $result;
    }



}