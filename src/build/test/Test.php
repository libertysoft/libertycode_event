<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load test
require_once($strRootAppPath . '/src/build/test/EventBuilderTest.php');

// Use
use liberty_code\event\event\api\EventInterface;
use liberty_code\event\event\model\DefaultEventCollection;



// Init var
$objEventCollection = new DefaultEventCollection($objCallFactory);

$tabDataSrc = array(
    'event_1_not_care' => [
        'key' => 'event_1',
        'call' => [
            'class_path_pattern' => 'liberty_code\\event\\event\\test\\ControllerTest1:action',
            //'class_path_format_require' => true
        ]
    ],
    'event_2' => [
        'type' => 'default',
        'name' => [],
        'call' => [
            'type' => 'dependency',
            'dependency_key_pattern' => 'svc_1:action'
        ]
    ],
	'event_3' => [
        'name' => ['event-a', 'event-b'],
        'call' => [
            'type' => 'file',
            'file_path_pattern' => 'src/event/test/FileControllerTest1.php',
            'file_path_format_require' => 7
        ]
	],
    [
        'type' => 'default',
        'name' => ['event-a', 'event-c'],
        'call' => [
            'type' => 'function',
            'file_path_pattern' => '/src/event/test/FileControllerTest2.php:runAction',
            'file_path_format_require' => true
        ]
    ]
);



// Test properties
echo('Test properties: <br />');
echo('Data source initialization: <pre>');print_r($objEventBuilder->getTabDataSrc());echo('</pre>');

echo('<br />');

$objEventBuilder->setTabDataSrc($tabDataSrc);
echo('Data source hydrated: <pre>');print_r($objEventBuilder->getTabDataSrc());echo('</pre>');

echo('<br />');

echo('<br /><br /><br />');



// Test hydrate event collection
echo('Test hydrate event collection: <br />');

$objEventBuilder->hydrateEventCollection($objEventCollection);
foreach($objEventCollection as $strKey => $objEvent)
{
    /** @var EventInterface $objEvent */
	try{
		echo('Event "' . $strKey . '":');echo('<br />');
		echo('Get: key: <pre>');print_r($objEvent->getStrKey());echo('</pre>');
        echo('Get: name: <pre>');var_dump($objEvent->getTabName());echo('</pre>');
		echo('Get: config: <pre>');print_r($objEvent->getTabConfig());echo('</pre>');
		echo('Get: event collection count: <pre>');print_r(count($objEvent->getObjEventCollection()->getTabKey()));echo('</pre>');
        echo('Get: instance: <pre>');var_dump(get_class($objEvent));echo('</pre>');
	} catch(\Exception $e) {
		echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
		echo('<br />');
	}
	
	echo('<br /><br />');
}

echo('<br /><br /><br />');



// Test clear event collection
echo('Test clear: <br />');

echo('Before clearing: <pre>');print_r($objEventCollection->getTabKey());echo('</pre>');

$objEventBuilder->setTabDataSrc(array());
$objEventBuilder->hydrateEventCollection($objEventCollection);
echo('After clearing: <pre>');print_r($objEventCollection->getTabKey());echo('</pre>');

echo('<br /><br /><br />');


