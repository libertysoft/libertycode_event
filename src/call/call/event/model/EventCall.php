<?php
/**
 * Description :
 * This class allows to define event call class.
 * Event call is a default event call, allows to configure event destination.
 *
 * Event call uses the following specified configuration:
 * [
 *     Default event call configuration
 * ]
 *
 * Event call uses the following specified destination configuration:
 * [
 *     event_key_pattern(required):
 *         "event key pattern,
 *         where '%N$s' or '%s' replaced by provided array of string elements, if required"
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\event\call\call\event\model;

use liberty_code\event\call\call\model\DefaultCall;

use liberty_code\call\call\library\ConstCall;
use liberty_code\event\call\call\event\library\ConstEventCall;
use liberty_code\event\call\call\event\exception\CallConfigInvalidFormatException;



class EventCall extends DefaultCall
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        // $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstCall::DATA_KEY_DEFAULT_CALL_CONFIG:
                    CallConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     * @throws CallConfigInvalidFormatException
     */
    public function getCallable(
        array $tabArg = array(),
        array $tabStrElm = array(),
        $boolCallBefore = true,
        $boolCallAfter = true
    )
    {
        // Init var
        $result = null;
        $objObserver = $this->getObjObserver();
        $tabCallConfig = $this->getTabCallConfig();

        // Check minimal requirement
        if((!is_null($objObserver)) && (count($tabCallConfig) > 0))
        {
            // Get infos from config
            $strEventKey = $tabCallConfig[ConstEventCall::TAB_CALL_CONFIG_KEY_EVENT_KEY_PATTERN]; // Get event key

            // Check config info valid
            if(
                (!is_null($strEventKey)) &&
                is_string($strEventKey) &&
                (trim($strEventKey) != '')
            )
            {
                // Get calculated event key
                $strEventKey = @$this->getStrValueFormatFromElm($strEventKey, $tabStrElm);

                // Check event found
                if ($objObserver->checkExists($strEventKey))
                {
                    // Set callback function
                    $result = function () use ($objObserver, $strEventKey, $tabStrElm, $tabArg)
                    {
                        // Return result
                        return $objObserver->executeEvent($strEventKey, $tabArg, $tabStrElm);
                    };

                    // Wrap callback function
                    $result = $this->getCallableWrap
                    (
                        $result,
                        array(
                            ConstEventCall::TAB_CALLABLE_PARAM_ARG_KEY_EVENT_KEY => $strEventKey,
                            ConstEventCall::TAB_CALLABLE_PARAM_ARG_KEY_CALL_ELEMENT => $tabStrElm,
                            ConstEventCall::TAB_CALLABLE_PARAM_ARG_KEY_CALL_ARGUMENT => $tabArg
                        ),
                        $boolCallBefore,
                        $boolCallAfter
                    );
                }
            }

            // Throw exception, if callable not gettable
            if(is_null($result))
            {
                throw new CallConfigInvalidFormatException(serialize($this->getTabCallConfig()));
            }
        }

        // Return result
        return $result;
    }



}