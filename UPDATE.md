LibertyCode_Event
=================



Version "1.0.0"
---------------

- Create repository

- Set event

- Set builder

- Set observer

- Set call

---



Version "1.0.1"
---------------

- Update event

    - Use call
    - Use event name
    - Set event factory
    
- Update builder
    
    - Use event factory

- Update observer

    - Use event name

---


