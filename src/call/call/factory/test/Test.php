<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../../..';

// Load test
require_once($strRootAppPath . '/src/call/call/factory/test/CallFactoryTest.php');

// Use
use liberty_code\call\call\file\model\FileCall;
use liberty_code\call\call\factory\library\ConstCallFactory;
use liberty_code\event\call\call\event\library\ConstEventCall;
use liberty_code\event\call\call\event\model\EventCall;
use liberty_code\event\call\call\factory\library\ConstEventCallFactory;



// Init var
$tabDataSrc = array(
    'event_1' => [
        'call' => [
            'class_path_pattern' => 'liberty_code\\event\\event\\test\\ControllerTest1:action',
            //'class_path_format_require' => true
        ]
    ]
);

$objEventBuilder->setTabDataSrc($tabDataSrc);
$objEventBuilder->hydrateEventCollection($objEventCollection);



// Test new call
$tabCallData = array(
    [
        [
            ConstCallFactory::TAB_CALL_CONFIG_KEY_TYPE => ConstEventCallFactory::CALL_CONFIG_TYPE_EVENT,
            ConstEventCall::TAB_CALL_CONFIG_KEY_EVENT_KEY_PATTERN => 'event_1'
        ]
    ], // Ok

    [
        [
            ConstCallFactory::TAB_CALL_CONFIG_KEY_TYPE => ConstEventCallFactory::CALL_CONFIG_TYPE_EVENT,
            'test' => 'action'
        ]
    ], // Ko: Function call invalid

    [
        [
            ConstCallFactory::TAB_CALL_CONFIG_KEY_TYPE => ConstEventCallFactory::CALL_CONFIG_TYPE_EVENT,
            ConstEventCall::TAB_CALL_CONFIG_KEY_EVENT_KEY_PATTERN => 'event_1'
        ],
        new EventCall($objObserver)
    ], // Ok

    [
        [
            ConstCallFactory::TAB_CALL_CONFIG_KEY_TYPE => ConstEventCallFactory::CALL_CONFIG_TYPE_EVENT,
            ConstEventCall::TAB_CALL_CONFIG_KEY_EVENT_KEY_PATTERN => 'route_1'
        ],
        new FileCall()
    ] // Ko: not found: file call used for event call config
);

foreach($tabCallData as $callData)
{
    echo('Test new call: <br />');
    echo('<pre>');var_dump($callData);echo('</pre>');

    try{
        $tabCallConfig = $callData[0];
        $objCall = (isset($callData[1]) ? $callData[1] : null);
        $objCall = $objCallFactory->getObjCall($tabCallConfig, $objCall);

        echo('Class path: <pre>');var_dump($objCallFactory->getStrCallClassPath($tabCallConfig));echo('</pre>');

        if(!is_null($objCall))
        {
            echo('Call class path: <pre>');var_dump(get_class($objCall));echo('</pre>');
            echo('Call config: <pre>');var_dump($objCall->getTabConfig());echo('</pre>');
            echo('Call destination config: <pre>');var_dump($objCall->getTabCallConfig());echo('</pre>');
            echo('Call callable before: <pre>');var_dump($objCall->getCallableBefore());echo('</pre>');
            echo('Call callable after: <pre>');var_dump($objCall->getCallableAfter());echo('</pre>');
        }
        else
        {
            echo('Call not found<br />');
        }

    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . ':' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');


